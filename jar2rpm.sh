#!/bin/bash

usage() { echo "Usage: $1 $2"; exit 1; }
error() { echo "$1"; exit 1; }

test $# -ne 1 && usage `basename $0` "<app>.[jar|ear|war]"

rpm -q rpmdevtools &>/dev/null || error 'Install the rpmdevtools package first.'

jarfile=$1

rpmver=1
rpmrel=1

test -f $jarfile || error 'The argument must be a file.'
case ${jarfile: -4} in
	.jar) extension='.jar';;
	.war) extension='.war';;
	.ear) extension='.ear';;
	*) error 'We expect a .jar/war/ear';;
esac

file $jarfile | grep -i 'archive data'
test $? -eq 0 || error 'The argument must be an jar archive'

rpm -q rpmdevtools &>/dev/null || yum -y install rpmdevtools
rpmdev-setuptree

jar=$(basename $jarfile $extension)

jarver=$(echo $jar | egrep -o '([0-9][0-9\.jdbc]+)$')
jarname=$(basename $jar "-${jarver}")

test -z $jarname && error 'Failed to parse the jar name'


mkdir -p ~/rpmbuild/SOURCES/$jarname-$jarver/jee-archives
cp $jarfile ~/rpmbuild/SOURCES/$jarname-$jarver/jee-archives
cd ~/rpmbuild/SOURCES/
tar cvzf $jarname-$jarver.tar.gz $jarname-$jarver
cd -

cat << 'EOF' > ~/rpmbuild/SPECS/$jarname-$jarver.spec
Name: <name>-<version>
Version: 1
Release: 1
Summary: <name>

Group: Development/Libraries
License: Unspecified
Source0: %{name}.tar.gz
BuildArchitectures:  noarch

%description
<name>

%prep

%setup -n %{name}

%build


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/opt/jboss-eap/resources/<name>-<version>/
cp -ra * $RPM_BUILD_ROOT/opt/jboss-eap/resources/<name>-<version>/

%clean
rm -rf $RPM_BUILD_ROOT

%pre
mkdir -p /opt/jboss-eap/resources

%files
%defattr(-,root,root,-)
/opt/jboss-eap/resources/<name>-<version>/*

%changelog
EOF

sed -i "s/Version: .*/Version: $rpmver/g" ~/rpmbuild/SPECS/$jarname-$jarver.spec
sed -i "s/Release: .*/Release: $rpmrel/g" ~/rpmbuild/SPECS/$jarname-$jarver.spec
sed -i "s/<name>/$jarname/g" ~/rpmbuild/SPECS/$jarname-$jarver.spec
sed -i "s/<version>/$jarver/g" ~/rpmbuild/SPECS/$jarname-$jarver.spec

cd ~/rpmbuild/SPECS
rpmbuild -ba $jarname-$jarver.spec
cd -

find ~/rpmbuild/ -name "$jarname-$jarver*.rpm"

